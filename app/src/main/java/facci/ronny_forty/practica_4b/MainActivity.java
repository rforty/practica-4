package facci.ronny_forty.practica_4b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    //Creamos la instancia del RecyclerView para proceder a agregar funcionalidad
    RecyclerView recyclerView = findViewById(R.id.recycler_view);

    //Creamos una lista de tipo String que representan el titulo
    //de cada item en el RecyclerView
    ArrayList<String> tareas =new ArrayList<>();

    //Agregamos items a la lista
    for(int item =0; item<=10; item++){
        tareas.add("Item "+item);
    }

    //Asignamos la lista al adapatador que creamos
    TareasRecyclerViewAdapter adapter =new TareasRecyclerViewAdapter(this, tareas);
    //Asignammos el adaptador al RecyclerView
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }
}
